from django.shortcuts import render
from datetime import datetime   
from .models import Plant
from .models import Advice
from .models import Offer
from .forms import AddOfferForm
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
import os


def index(request):
    return render(request, 'pages/index.html')


def login(request):
    return render(request, 'pages/login.html')


def plant_list(request):
    return render(request, 'pages/plant_list.html')

def plant_detail(request, plant_id):
    return render(request, 'pages/plant_detail.html')

def offer_list(request):
    return render(request, 'pages/offer_list.html')

def offer_detail(request, offer_id):
    return render(request, 'pages/offer_detail.html')

def offer_add(request):
    # Users nécessaires :
    try:
        gardien = User.objects.get(username='Gardien')
    except User.DoesNotExist:
        gardien = User.objects.create_user(username='Gardien', email='jardinage@example.com', password='gazon')
        gardien.save()
    try:
        jm = User.objects.get(username='Jean Margaux')
    except User.DoesNotExist:
        jm = User.objects.create_user(username='Jean Margaux', email='potajet@example.com', password='pelouz')
        jm.save()
    # Traitement formulaire :
    if request.method == 'POST':
        fs = FileSystemStorage()
        titre = request.POST.get('titre')
        desc = request.POST.get('description')

        offer = Offer(name=titre,date=datetime.now(),description=desc,guardian=gardien,owner=jm)
        offer.save()
        file = request.FILES['photo']
        nom_fichier = fs.save(os.path.join('images', file.name), file)
        os.rename(nom_fichier,'images/offer_'+str(offer.id)+'.jpg')
        return render(request, 'pages/offer_list.html')
    # Envoie formulaire :
    form = AddOfferForm()
    return render(request, 'pages/offer_add.html', {'form': form})

def user_detail(request, user_id):
    print(user_id)
    user = User.objects.get(id=user_id)
    print(f'User : { user }')

    offers = Offer.objects.filter(owner=user)
    print(offers)

    
    # offer = Offer.objects.get(owner=user_id)
    # print(f'Offer : { offer }')
        
    context = {
        'user': user,
        'offers': offers
    }
    return render(request, 'pages/user_detail.html', context)
from django import forms

class AddOfferForm(forms.Form):
    titre = forms.CharField(label='Titre ', max_length=100)
    description = forms.CharField(label='Description ',max_length=300)
    photo = forms.FileField(label='Photo ')